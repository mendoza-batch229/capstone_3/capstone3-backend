// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userControllers = require("../controllers/usercontrollers.js");

/*==================================================================================================*/
// Check email if it is existing to our database
router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// User Registration route
router.post("/register", (req, res) => {
	userControllers.regUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User LogIn
router.post("/logIn", (req, res) => {
	userControllers.logIn(req.body).then(resultFromController => res.send(resultFromController));
})

// Retrieve ALL USER
router.get("/all", (req, res) => {

	userControllers.getAllUserInfo().then(resultFromController => res.send(resultFromController));
});


// RETRIEVE USER DETAILS
router.get("/details", (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	userControllers.getUserInfo({userId : userData.id}).then(resultFromController => res.send(resultFromController));

// 	userControllers.getUserInfo().then(resultFromController => res.send(resultFromController));
});



// CREATE ORDER
router.post("/pOrder", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }

    
	userControllers.pOrder(data).then(resultFromController => res.send(resultFromController));
})






// SET ADMIN USER
router.put("/:userId/admin", auth.verify, (req, res) => {
	
	const data = {
		adminreq: req.body,
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAdminUser(data).then(resultFromController => res.send(resultFromController));
	
});


// ADD2CART
router.post("/add2cart", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }

    
	userControllers.cart(data).then(resultFromController => res.send(resultFromController));
})

/*==================================================================================================*/
module.exports = router;
// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js")

const productControllers = require ("../controllers/productcontrollers.js")
/*==================================================================================================*/

// User Registration route
router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productControllers.addProduct(data).then(resultFromController => res.send(resultFromController));

});



// Retrieve all Prducts
router.get("/all", (req, res) => {

	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// RETRIEVE SINGLE PRODUCT
router.get("/:productId", (req, res) => {

	productControllers.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// UPDATE PRODUCTS ADMIN
// router.put("/:productId", auth.verify, (req, res) => {
router.put("/productId", auth.verify, (req, res) => {
	
	const data = {
		// reqParams: req.params,
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.updateProduct(data).then(resultFromController => res.send(resultFromController));
	
});
// UPDATE(2) PRODUCTS ADMIN
router.put("/:productId", auth.verify, (req, res) => {
	
	const data = {
		reqParams: req.params,
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.updateProduct1(data).then(resultFromController => res.send(resultFromController));
	
});



// archiving a PRODUCTS
router.put("/:productId/archive", auth.verify, (req, res) => {
	
	const data = {
		reqParams: req.params,
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(data).then(resultFromController => res.send(resultFromController));
	
});






/*==================================================================================================*/
module.exports = router;
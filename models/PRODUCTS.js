const mongoose = require ("mongoose");

// Blueprints of my DB
/*==================================================================================================*/

const productSchema = new mongoose.Schema({
	imageURL:{
		type: String,
		required:[true, "Product Image requires"]
	},
	name:{
		type: String,
		required:[true, "Product name requires"]
	},
	description:{
		type: String,
		required:[true, "Desc required"]
	},
	price:{
		type:Number,
		required:[true, "Price required"]
	},
	isActive:{
		type: Boolean,
	default: true
	},
	createOn:{
		type: Date,
	default: new Date()
	},
	orders:[{
		orderId:{
			type: String,
			required:[true, "Order Id is required"]
		}
	}]



});

module.exports = mongoose.model("PRODUCT", productSchema);
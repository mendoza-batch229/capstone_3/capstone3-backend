const mongoose = require("mongoose");

// Blueprints of my DB
/*==================================================================================================*/

const userSchema = new mongoose.Schema({
	fName: {
		type: String,
		required: [true, "firstname is required"]
	},
	lName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders:[{
		products:[{
			imageURL:{
			type: String,
			required: [true, "URL is required!"]
			},
			pName:{
			type: String,
			required: [true, "Product name is required!"]
			},
			pDesc:{
				type: String,
				required: [true, "Description is required!"]
				},
			pPrice:{
				type: Number,
				required: [true, "Price is required!"]
				},
			pQuantity:{
			type: Number,
			required: [true, "Quantity is required!"]
			}
		}],
		totalAmount:{
			type: Number,
			required:[false, "Total required"]
		},
		purchaseOn:{
			type: Date,
			default: new Date()
		}
	}],
	cart:[{
		products:[{
			imageURL:{
			type: String,
			required: [true, "URL is required!"]
			},
			pName:{
			type: String,
			required: [true, "Product name is required!"]
			},
			pDesc:{
				type: String,
				required: [true, "Description is required!"]
				},
			pPrice:{
				type: Number,
				required: [true, "Price is required!"]
				},
			pQuantity:{
			type: Number,
			required: [true, "Quantity is required!"]
			}
		}],
		totalAmount:{
			type: Number,
			required:[false, "Total required"]
		}

	}]

});

module.exports = mongoose.model("USER", userSchema);
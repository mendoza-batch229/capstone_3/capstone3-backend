const USER = require("../models/USERS.js");
const PRODUCT = require("../models/PRODUCTS.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// code blocks all needed action put here
/*==================================================================================================*/
// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return USER.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}
//User Registration 
module.exports.regUser = (reqBody) =>{
	let newUser = new USER({
		fName: reqBody.fName,
		lName: reqBody.lName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((save,err) =>{
		if(err){
			return false;
		}else{
			return true;
		}
	})
};



// User LogIn
module.exports.logIn = (reqBody) => {
	return USER.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			// return true;
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};



// RETRIEVE ALL USER DETAILS 
module.exports.getAllUserInfo = () => {
	return USER.find({}).then(result => {
		return result;
	});
};



// RETRIEVE USER DETAILS 
module.exports.getUserInfo = (data) => {

	return USER.findById(data.userId).then(result => {
		result.password = undefined;
		return result;
	})
};





	


// checkout
module.exports.pOrder = async (data) => {
	    	
    if(data.user != null){

        if(data.user.isAdmin) {
            return false;
        } else {
        	console.log(data.user.isAdmin);
        	const product = await PRODUCT.findOne({name:data.order.pName});
        	console.log(product);

        	let {imageURL, name, description ,price} = product
			console.log(imageURL);
        	console.log(name);
			console.log(description);
        	console.log(price);

            const user = await USER.findOne({email:data.user.email});
            

			        let newOrder = {
			products: [
			    {
					imageURL: imageURL,
			        pName: name,
					pDesc: description,
					pPrice: price,
			        pQuantity: data.order.pQuantity
			    }
			],

			
			totalAmount: price * data.order.pQuantity,
			
			purchaseOn: new Date()
			}
			user.orders.push(newOrder);
			console.log(user);

			return user.save().then((save, err) =>{
				if(err){
					return false;
				}else{
					return true;
				}
			})
        }

    } else {
        return false
    }
} 




// checking if the user is an admin update user set as admin
module.exports.setAdminUser = async (data) => {

// checking if the user is an admin
if(data.isAdmin === true){

		let updateAdminField = {
			isAdmin : data.adminreq.isAdmin
		};

		return await USER.findByIdAndUpdate(data.reqParams.userId, updateAdminField).then((course, error) => {

			if (error) {

				return false;

			} else {

				return true;
			}
		});
	} else {
		return false;
	};
};


// ADD to CART
module.exports.cart = async (data) => {
	    	
    if(data.user != null){

        if(data.user.isAdmin) {
            return false;
        } else {
        	console.log(data.user.isAdmin);
        	const product = await PRODUCT.findOne({name:data.order.pName});
        	console.log(product);

        	let {name,price} = product
        	console.log(name);
        	console.log(price);

            const user = await USER.findOne({email:data.user.email});
            

			        let newCart = {
			products: [
			    {
			        pName: name,
			        pQuantity: data.order.pQuantity
			    }
			],

			
			totalAmount: price * data.order.pQuantity,
			
			
			}
			user.orders.push(newCart);
			console.log(user);

			return user.save().then((save, err) =>{
				if(err){
					return false;
				}else{
					return true;
				}
			})
        }

    } else {
        return false
    }
} 
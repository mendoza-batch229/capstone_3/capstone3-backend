const PRODUCT = require("../models/PRODUCTS.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// code blocks all needed action put here
/*==================================================================================================*/

// CREATE PRODUCT
module.exports.addProduct = async (data) =>{
	if(data.isAdmin){

	let addProd = new PRODUCT({
	imageURL: data.products.imageURL,
	name: data.products.name,
	description: data.products.description,
	price: data.products.price
});
	return await addProd.save().then((save,err) =>{
	if(err){
		return false;
	}else{
		return true;
	}
	});
	
	} else {
		return false;
	}
};


// RETRIEVE ALL PRODUCTS
module.exports.getAllProducts = async () => {

		return await PRODUCT.find({}).then(result => {
		return result;
	});


};


// RETRIEVE SINGLE PRODUCT 
module.exports.getProduct = (reqParams) => {
	console.log(reqParams)
	return PRODUCT.findById(reqParams.productId).then(result => {
		return result;
	});
};



// UPDATE PRODUCT
module.exports.updateProduct = async(data) => {
	if(data.isAdmin){
	let updatedProduct = {
		imageURL: data.reqBody.imageURL,
		name : data.reqBody.name,
		description: data.reqBody.description,
		price: data.reqBody.price
	};


	return await PRODUCT.findByIdAndUpdate(data.reqBody.productId, updatedProduct).then((course, err) => {
		if(err){
			return false;
	
		} else {
			return true;
		}
	});

 }else{
 	return false;
 }

};

// UPDATE(2) PRODUCT
module.exports.updateProduct1 = async(data) => {
	if(data.isAdmin){
	let updatedProduct = {
		imageURL: data.reqBody.imageURL,
		name : data.reqBody.name,
		description: data.reqBody.description,
		price: data.reqBody.price
	};


	return await PRODUCT.findByIdAndUpdate(data.reqParams.productId, updatedProduct).then((course, err) => {
		if(err){
			return false;
	
		} else {
			return true;
		}
	});

 }else{
 	return false;
 }

};


module.exports.archiveProduct = async (data) => {

// checking if the user is an admin
if(data.isAdmin === true){

		let updateActiveField = {
			isActive : data.reqBody.isActive
		};

		return await PRODUCT.findByIdAndUpdate(data.reqParams.productId, updateActiveField).then((course, error) => {

			if (error) {

				return false;

			} else {

				return true;
			}
		});
	} else {
		return false;
	};
};





